// Primary
import 'package:flutter/material.dart';

///Diarium Design Color NEWWWW
const backgroundColor = tertiary100;

const headingText = const Color(0xff2D3748);
const bodyText = const Color(0xff4A5568);
const captionText = const Color(0xff718096);
const disableText = const Color(0xffA0AEC0);
const iconDisabled = const Color(0xffA0AEC0);
const placeholderText = const Color(0xffE2E8F0);
const border = const Color(0xffE2E8F0);
const divider = const Color(0xffEDF2F7);
const accentInformation200 = const Color(0xffBEE3F8);
const accentInformation600 = const Color(0xff3182CE);
const redBase = const Color(0xffBE1027);

const primary100 = const Color(0xffE6FFFA);
const primary200 = const Color(0xffB2F5EA);
const primary300 = const Color(0xff81E6D9);
const primary400 = const Color(0xff4FD1C5);
const primary500 = const Color(0xff38B2AC);
const primary600 = const Color.fromARGB(255, 0, 75, 171);
// const primary600 = const Color(0xff319795);

const primary700 = const Color(0xff2C7A7B);
const primary800 = const Color(0xff285E61);
const primary900 = const Color(0xff234E52);

const secondary100 = const Color(0xffFFF5F5);
const secondary200 = const Color(0xffFED7D7);
const secondary300 = const Color(0xffFEB2B2);
const secondary400 = const Color(0xffFC8181);
const secondary500 = const Color(0xffF56565);
const secondary600 = const Color(0xffE53E3E);
const secondary700 = const Color(0xffC53030);
const secondary800 = const Color(0xff9B2C2C);
const secondary900 = const Color(0xff742A2A);

const tertiary100 = const Color(0xffF7FAFC);
const tertiary200 = const Color(0xffEDF2F7);
const tertiary300 = const Color(0xffE2E8F0);
const tertiary400 = const Color(0xffCBD5E0);
const tertiary500 = const Color(0xffA0AEC0);
const tertiary600 = const Color(0xff718096);
const tertiary700 = const Color(0x5F5458);
const tertiary800 = const Color(0xff2D3748);
const tertiary900 = const Color(0xff1A202C);

const information100 = const Color(0xffEBF8FF);
const information200 = const Color(0xffBEE3F8);
const information300 = const Color(0xff90CDF4);
const information500 = const Color(0xff4299E1);
const information600 = const Color(0xff3182CE);
const information700 = const Color(0xff2B6CB0);
const information800 = const Color(0xff2C5282);
const information900 = const Color(0xff2A4365);

const warning100 = const Color(0xffFFFFF0);
const error100 = const Color(0xffFFFAF0);
const error200 = const Color(0xffFEEBC8);
const error300 = const Color(0xffFBD38D);
const error400 = const Color(0xffF6AD55);
const error500 = const Color(0xffED8936);
const error600 = const Color(0xffDD6B20);
const error700 = const Color(0xffC05621);
const error800 = const Color(0xff9C4221);
const error900 = const Color(0xff7B341E);

const warning200 = const Color(0xffFEFCBF);
const warning300 = const Color(0xffFAF089);
const warning400 = const Color(0xffF6E05E);
const warning500 = const Color(0xffECC94B);
const warning600 = const Color(0x5F5458);
const warning700 = Color.fromARGB(95, 84, 88, 1);
const warning800 = const Color(0xff975A16);
const warning900 = const Color(0xff744210);

const success100 = const Color(0xffF0FFF4);
const success200 = const Color(0xffC6F6D5);
const success300 = const Color(0xff9AE6B4);
const success400 = const Color(0xff68D391);
const success500 = const Color(0xff48BB78);
const success600 = const Color(0xff38A169);
const success700 = const Color(0xff2F855A);
const success800 = const Color(0xff276749);
const success900 = const Color(0xff22543D);

const black = const Color(0xff000000);
const white = const Color(0xffFFFFFF);
const additionalColorBlack = const Color(0xff000000);
const additionalColorWhite = const Color(0xffFFFFFF);
const addtionalColorPink = const Color(0xFFFBB6CE);
const addtionalColorPink200 = const Color(0xFFFED7E2);
const addtionalColorPurle500 = const Color(0XFF9F7AEA);
const addtionalColorPurple = const Color(0xFFD6BCFA);
const polri700 = Color.fromRGBO(95, 84, 88, 1);
const polri100 = Color.fromRGBO(223, 221, 222, 1);

const blue600 = Color.fromARGB(255, 0, 75, 171);
const bgColor = Color(0xFF19182C);

const starColor = Color(0xFFFFAB2E);
