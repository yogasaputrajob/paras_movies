import 'package:hive_flutter/hive_flutter.dart';
import 'package:paras/modules/homepage/model/draft_fav_movie_model.dart';

void registerHiveAdapter() {
  Hive.registerAdapter(DraftFavMovieModelAdapter());
}
