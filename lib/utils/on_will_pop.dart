import 'dart:io';

import 'package:paras/lib.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

DateTime? backButtonPressedTime;

Future<bool> onWillPop() async {
  DateTime currentTime = DateTime.now();
  //Statement 1 Or statement2
  bool backButton = backButtonPressedTime == null ||
      currentTime.difference(backButtonPressedTime!) > Duration(seconds: 1);
  if (backButton) {
    backButtonPressedTime = currentTime;
    Fluttertoast.showToast(
        msg: "Double Click to exit app",
        backgroundColor: Colors.black38,
        textColor: Colors.white);
    return false;
  }
  exit(0);
}

Future<bool> onWillBack({BuildContext? c}) async {
  print("BACK");
  return false;
}
