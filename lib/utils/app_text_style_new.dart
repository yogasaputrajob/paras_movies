import 'package:flutter/material.dart';

import 'package:paras/lib.dart';

const text16WhiteNunitoBold = TextStyle(
  fontFamily: 'Nunito',
  color: white,
  fontSize: 16,
  fontWeight: FontWeight.bold,
);

const text14NunitoBlackW500Underline = TextStyle(
  fontFamily: 'Nunito',
  color: black,
  fontSize: 14,
  fontWeight: FontWeight.w500,
  decoration: TextDecoration.underline,
);

const text14NunitoBlackW500 = TextStyle(
    fontFamily: 'Nunito',
    color: black,
    fontSize: 14,
    fontWeight: FontWeight.w500);

const text24T800W700 = TextStyle(
  fontFamily: 'Nunito',
  color: tertiary800,
  fontWeight: FontWeight.w700,
  fontSize: 24,
);

const text26NunitoBlackBold = TextStyle(
    fontFamily: 'Nunito',
    color: black,
    fontSize: 26,
    fontWeight: FontWeight.bold);

const text18NunitoWhiteW500 = TextStyle(
    fontFamily: 'Nunito',
    color: white,
    fontSize: 18,
    fontWeight: FontWeight.w500);

const text24NunitoWhiteBold = TextStyle(
    fontFamily: 'Nunito',
    color: white,
    fontSize: 24,
    fontWeight: FontWeight.bold);

const text16HTW600 = TextStyle(
  fontFamily: 'Nunito',
  color: headingText,
  fontWeight: FontWeight.w600,
  fontSize: 16,
);

const text14SC500W600 = TextStyle(
  fontFamily: 'Nunito',
  color: success500,
  fontWeight: FontWeight.w600,
  fontSize: 14,
);

const text34NunitoWhiteNormal = TextStyle(
    fontFamily: 'Nunito',
    color: white,
    fontWeight: FontWeight.w700,
    fontSize: 34);

const text20NunitoWhiteW700 = TextStyle(
    fontFamily: 'Nunito',
    color: white,
    fontSize: 20,
    fontWeight: FontWeight.w500);

const text16NunitoWhiteW700 = TextStyle(
    fontFamily: 'Nunito',
    color: white,
    fontSize: 16,
    fontWeight: FontWeight.w700);

const text16NunitoWhiteNormal = TextStyle(
  fontFamily: 'Nunito',
  color: white,
  fontSize: 16,
);

const text16BTW400 = TextStyle(
  fontFamily: 'Nunito',
  color: bodyText,
  fontWeight: FontWeight.w400,
  fontSize: 16,
);

const text14whiteHTW600 = TextStyle(
  fontFamily: 'Nunito',
  color: white,
  fontWeight: FontWeight.w600,
  fontSize: 14,
);
