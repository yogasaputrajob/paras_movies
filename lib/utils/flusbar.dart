import 'package:another_flushbar/flushbar.dart';
import 'package:paras/lib.dart';
import 'package:flutter/material.dart';

class FlushBar {
  static void flushBarView({
    VoidCallback? callBack,
    required BuildContext? context,
    required String? message,
    bool? showIcon = true,
    Color? iconColor = error500,
    Color? backgroundColor,
    TextStyle? textStyle,
    IconData? icon = Icons.info_outline,
    Widget? button,
    int duration = 2,
    double marginHorizonal = 8,
    double marginVertical = 8,
    FlushbarPosition position = FlushbarPosition.TOP,
    bool? isDismissible = false,
  }) {
    new Flushbar(
      isDismissible: isDismissible!,
      boxShadows: [
        BoxShadow(
          color: Colors.grey,
          blurRadius: 4.0,
        ),
      ],
      margin: EdgeInsets.symmetric(
          horizontal: marginHorizonal, vertical: marginVertical),
      mainButton: button != null ? button : Container(),
      borderRadius: BorderRadius.circular(16),
      backgroundColor: backgroundColor == null ? white : backgroundColor,
      flushbarPosition: position,
      messageText: Text(
        message!,
        style: textStyle == null ? text14NunitoBlackW500 : textStyle,
      ),
      icon: showIcon!
          ? Icon(
              icon,
              size: 28.0,
              color: iconColor == null ? redBase : iconColor,
            )
          : Container(),
      duration: Duration(seconds: duration),
    )..show(context!).then((value) {
        if (callBack != null) {
          callBack();
        }
      });
  }
}
