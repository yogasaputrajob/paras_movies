import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:page_transition/page_transition.dart';
import 'package:paras/lib.dart';
import 'package:paras/modules/bottom_navigation/cubit/bottom_navigation_cubit.dart';
import 'package:paras/modules/bottom_navigation/page/bottom_navigation_page.dart';
import 'package:paras/modules/profile/profile.dart';

/// Navigation Class
const routeLogin = "/";
const routeDetailMovie = "/route_detail";
const routeLoginPage = "/route_login";
const routeProfilePage = "/route_profile";
const routeSplashScreen = "/route_splash_screen";
const routeHome = "/route_home";
const routeHomePage = "/route_home_page";
// ignore: missing_return
Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case routeSplashScreen:
      Widget _builder = SplashScreenPage();
      if (Platform.isIOS) {
        return CupertinoPageRoute(builder: (context) => _builder);
      } else {
        return PageTransition(
            child: Builder(builder: (context) => _builder),
            type: PageTransitionType.fade);
      }

    case routeLoginPage:
      Widget _builder = LoginPage();
      if (Platform.isIOS) {
        return CupertinoPageRoute(builder: (context) => _builder);
      } else {
        return PageTransition(
            child: Builder(builder: (context) => _builder),
            type: PageTransitionType.fade);
      }

    case routeProfilePage:
      Widget _builder = ProfileScreenPage();
      if (Platform.isIOS) {
        return CupertinoPageRoute(builder: (context) => _builder);
      } else {
        return PageTransition(
            child: Builder(builder: (context) => _builder),
            type: PageTransitionType.fade);
      }

    case routeHomePage:
      Widget _builder = HomePage();
      if (Platform.isIOS) {
        return CupertinoPageRoute(builder: (context) => _builder);
      } else {
        return PageTransition(
            child: Builder(builder: (context) => _builder),
            type: PageTransitionType.fade);
      }
    case routeHome:
      Widget _builder = BlocProvider<BottomNavigationCubit>(
        create: (context) => BottomNavigationCubit(),
        child: BottomNav(),
      );
      if (Platform.isIOS) {
        return CupertinoPageRoute(builder: (context) => _builder);
      } else {
        return PageTransition(
            child: Builder(builder: (context) => _builder),
            type: PageTransitionType.fade);
      }
    case routeDetailMovie:
      Widget _builder = DetailPage(id: settings.arguments as int);
      if (Platform.isIOS) {
        return CupertinoPageRoute(builder: (context) => _builder);
      } else {
        return PageTransition(
            child: Builder(builder: (context) => _builder),
            type: PageTransitionType.fade);
      }
  }

  return PageTransition(child: LoginPage(), type: PageTransitionType.fade);
}
