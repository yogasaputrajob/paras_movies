import 'package:dio/dio.dart';

// ignore: non_constant_identifier_names
DioCatchError(e) {
  if (e.response != null) {
    return e.response;
  } else if (e.type == DioErrorType.connectTimeout) {
    return null;
  } else if (e.type == DioErrorType.receiveTimeout) {
    return null;
  } else {
    return null;
  }
}
