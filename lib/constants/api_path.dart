import 'package:paras/lib.dart';

String baseUrl = 'https://api.themoviedb.org/3/';
String baseImg = 'https://image.tmdb.org/t/p/w300_and_h450_bestv2';
const tokenKey = '3a341978f9582f4e53030947e77cc2e9';
const apiNowPlaying = "movie/now_playing/";
const apiPopular = "movie/popular/";
const apiDetailMovie = "movie/";
