import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:paras/modules/homepage/model/draft_fav_movie_model.dart';
import 'firebase_options.dart';
import 'package:paras/lib.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  await Hive.initFlutter();
  registerHiveAdapter();
  await Hive.openBox<DraftFavMovieModel>('draftMovie');

  runApp(new MaterialApp(
    initialRoute: routeLogin,
    onGenerateRoute: generateRoute,
    home: AppPage(),
  ));
}

class AppPage extends StatelessWidget {
  const AppPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SplashScreenPage(),
    );
  }
}
