import 'dart:convert';

import 'package:fluentui_system_icons/fluentui_system_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:paras/constants/api_path.dart';
import 'package:paras/lib.dart';
import 'package:hive/hive.dart';
import 'package:paras/modules/homepage/cubit/detail_cubit.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:paras/modules/homepage/model/draft_fav_movie_model.dart';

class DetailPage extends StatefulWidget {
  final int? id;
  DetailPage({Key? key, this.id}) : super(key: key);

  @override
  State<DetailPage> createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  DetailCubit _detailCubit = DetailCubit();
  bool isExpanded = false;
  bool isFav = false;
  DraftFavMovieModel _draftFavMovieModel = DraftFavMovieModel();
  DraftFavMovieModel _draftFavMovieModel2 = DraftFavMovieModel();
  @override
  void initState() {
    _detailCubit.getDetailMovie(id: widget.id);
    cekFavLokal();
    super.initState();
  }

  void cekFavLokal() async {
    Box box = await Hive.openBox<DraftFavMovieModel>("draftMovie");
    _draftFavMovieModel2 = box.get(widget.id);
    if (_draftFavMovieModel2.isFav == true) {
      setState(() {
        isFav = true;
      });
    } else {
      setState(() {
        isFav = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBarWidget(
          title: "Detail Movie",
          textStyle: text24NunitoWhiteBold,
          backgroundColor: bgColor,
          onPressed: () {
            Navigator.pop(context);
          },
          onPressedColor: white,
          actions: [
            InkWell(
                onTap: () async {
                  setState(() {
                    isFav == true ? isFav = false : isFav = true;
                  });
                  Box box =
                      await Hive.openBox<DraftFavMovieModel>("draftMovie");
                  if (isFav == true) {
                    box.put(widget.id, _draftFavMovieModel);
                  } else {
                    box.delete(widget.id);
                  }
                },
                child: isFav == false
                    ? Icon(FluentIcons.heart_24_regular, color: white, size: 24)
                    : Icon(FluentIcons.heart_24_filled,
                        color: redBase, size: 24)),
            AppDimens.horizontalSpace16
          ],
        ),
        body: Container(
            width: double.infinity,
            height: double.infinity,
            color: bgColor,
            child: BlocConsumer<DetailCubit, DetailState>(
                bloc: _detailCubit,
                listener: (c, s) {},
                builder: (c, s) {
                  if (s is OnDoneDetail) {
                    _draftFavMovieModel = DraftFavMovieModel(
                        id: s.data!.id,
                        isFav: true,
                        originalTitle: s.data!.originalTitle,
                        posterPath: "${baseImg}${s.data!.posterPath!}",
                        rating: s.data!.voteAverage.toString());
                    return Padding(
                      padding: const EdgeInsets.only(
                          top: 16.0, left: 16, right: 16, bottom: 16),
                      child: SingleChildScrollView(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              height: 400,
                              width: 310,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  image: DecorationImage(
                                      image: NetworkImage(
                                          "${baseImg}${s.data!.posterPath!}"),
                                      fit: BoxFit.fill)),
                            ),
                            AppDimens.verticalSpace12,
                            Row(
                              children: [
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 32.0, right: 32),
                                    child: Text(
                                      "${s.data!.originalTitle}",
                                      style: text20NunitoWhiteW700,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            AppDimens.verticalSpace8,
                            Row(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 32.0),
                                  child: Text(
                                    "Rating: ",
                                    style: text16NunitoWhiteW700,
                                  ),
                                ),
                                Icon(
                                  Icons.star,
                                  size: 16,
                                  color: starColor,
                                ),
                                Text(
                                  " ${s.data!.voteAverage}",
                                  style: text16NunitoWhiteW700,
                                ),
                              ],
                            ),
                            AppDimens.verticalSpace18,
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 32.0, right: 32),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: List.generate(s.data!.genres!.length,
                                    (index) {
                                  var dt = s.data!.genres![index];
                                  return Expanded(
                                    child: Container(
                                      margin: EdgeInsets.only(right: 12),
                                      padding: EdgeInsets.symmetric(
                                          vertical: 4, horizontal: 12),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(4),
                                        color: starColor.withOpacity(0.1),
                                      ),
                                      child: Text(
                                        "${dt.name}",
                                        style: text16NunitoWhiteNormal,
                                      ),
                                    ),
                                  );
                                }),
                              ),
                            ),
                            AppDimens.verticalSpace16,
                            Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 32.0),
                                  child: Row(
                                    children: [
                                      Text(
                                        "Overview",
                                        style: text24NunitoWhiteBold,
                                      ),
                                    ],
                                  ),
                                ),
                                AppDimens.verticalSpace10,
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 32.0, right: 32),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: RichText(
                                          text: TextSpan(
                                            text: '${s.data!.overview}',
                                            style: text16NunitoWhiteNormal,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    );
                  } else {
                    return Center(
                        child: CircularProgressIndicator(
                      color: white,
                    ));
                  }
                })));
  }
}
