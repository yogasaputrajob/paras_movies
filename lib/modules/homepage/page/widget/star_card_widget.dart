import 'package:flutter/material.dart';
import 'package:paras/utils/app_colors_new.dart';

class StarCard extends StatelessWidget {
  const StarCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Icon(
            Icons.star,
            size: 18,
            color: starColor,
          ),
          Icon(
            Icons.star,
            size: 18,
            color: starColor,
          ),
          Icon(
            Icons.star,
            size: 18,
            color: starColor,
          ),
          Icon(
            Icons.star,
            size: 18,
            color: starColor,
          ),
          Icon(
            Icons.star,
            size: 18,
            color: starColor,
          ),
        ],
      ),
    );
  }
}
