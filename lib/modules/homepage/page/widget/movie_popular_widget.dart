import 'package:flutter/material.dart';
import 'package:paras/modules/homepage/page/widget/star_card_widget.dart';
import 'package:paras/utils/utils.dart';

class MoviePopular extends StatelessWidget {
  final String name;
  final String title;
  final String imageUrl;
  const MoviePopular({
    Key? key,
    this.name = '',
    this.title = '',
    this.imageUrl = '',
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        bottom: 20,
      ),
      width: double.infinity,
      height: 127,
      child: Row(
        children: [
          Image.network(
            imageUrl,
            fit: BoxFit.cover,
            width: 110,
            height: 127,
          ),
          SizedBox(
            width: 20,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  name,
                  style: text16WhiteNunitoBold,
                  overflow: TextOverflow.fade,
                  maxLines: 1,
                  softWrap: false,
                ),
                SizedBox(
                  height: 1,
                ),
                Text(title, style: text16WhiteNunitoBold),
                SizedBox(
                  height: 20,
                ),
                StarCard(),
              ],
            ),
          )
        ],
      ),
    );
  }
}
