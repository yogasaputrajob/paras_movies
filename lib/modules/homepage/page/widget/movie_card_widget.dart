import 'package:flutter/material.dart';
import 'package:paras/lib.dart';
import 'package:paras/modules/homepage/page/widget/star_card_widget.dart';

class MovieCard extends StatelessWidget {
  final String name;
  final String titile;
  final String imageUrl;
  const MovieCard({
    Key? key,
    this.name = '',
    this.titile = '',
    this.imageUrl = '',
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        left: 16,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.network(imageUrl, fit: BoxFit.fitWidth),
          SizedBox(
            height: 26,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(name, style: text16WhiteNunitoBold),
              SizedBox(height: 1),
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 0.0),
                    child: Text(
                      "Rating: ",
                      style: text16BTW400,
                    ),
                  ),
                  Icon(
                    Icons.star,
                    size: 16,
                    color: starColor,
                  ),
                  Text(
                    " ${titile}",
                    style: text16BTW400,
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
