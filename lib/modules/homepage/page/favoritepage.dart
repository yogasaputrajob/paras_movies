import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:paras/lib.dart';
import 'package:paras/modules/homepage/model/draft_fav_movie_model.dart';
import 'package:paras/modules/login/services/google_auth_service.dart';

class FavoriteScreenPage extends StatefulWidget {
  final int? id;
  FavoriteScreenPage({Key? key, this.id}) : super(key: key);

  @override
  State<FavoriteScreenPage> createState() => _FavoriteScreenPageState();
}

class _FavoriteScreenPageState extends State<FavoriteScreenPage> {
  bool isExpanded = false;
  List<DraftFavMovieModel> _draftFavMovieModel = [];
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBarWidget(
          title: "Favorites Movie",
          textStyle: text24NunitoWhiteBold,
          backgroundColor: bgColor,
        ),
        body: Container(
            width: double.infinity,
            height: double.infinity,
            color: bgColor,
            child: ValueListenableBuilder(
              valueListenable:
                  Hive.box<DraftFavMovieModel>('draftMovie').listenable(),
              builder: (context, Box<DraftFavMovieModel> box, _) {
                if (box.values.isEmpty) {
                  return Center(child: Text('No movie on favorites'));
                } else {
                  return ListView.builder(
                    itemCount: box.values.length,
                    itemBuilder: (context, index) {
                      var contact = box.getAt(index);
                      return InkWell(
                        onTap: () {
                          Navigator.pushNamed(context, routeDetailMovie,
                              arguments: contact!.id!);
                        },
                        child: MoviePopular(
                          name: contact!.originalTitle!,
                          title: contact.rating!,
                          imageUrl: contact.posterPath!,
                        ),
                      );
                    },
                  );
                }
              },
            )));
  }
}
