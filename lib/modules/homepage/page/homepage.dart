import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:paras/constants/api_path.dart';
import 'package:paras/lib.dart';
import 'package:paras/modules/homepage/cubit/home_cubit.dart';
import 'package:paras/modules/homepage/cubit/home_trending_cubit.dart';
import 'package:paras/modules/homepage/model/home_nowplaying_model.dart';

import 'package:pull_to_refresh_flutter3/pull_to_refresh_flutter3.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  HomeCubit _homeCubit = HomeCubit();
  HomeTrendingCubit _homeTrendingCubit = HomeTrendingCubit();
  List<Result> _popularData = [];
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  int _offset = 1;
  bool _isRefreshing = false;
  bool _isDisableLoadMore = false;
  bool isTab = false;

  @override
  void initState() {
    _homeTrendingCubit.getListMovieTrending(page: _offset);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Container(
        color: bgColor,
        alignment: Alignment.topCenter,
        child: Column(
          children: [
            Expanded(
              child: SmartRefresher(
                  physics: BouncingScrollPhysics(),
                  enablePullDown: true,
                  enablePullUp: !_isDisableLoadMore,
                  header: MaterialClassicHeader(),
                  footer: ClassicFooter(
                    loadStyle: LoadStyle.ShowWhenLoading,
                    completeDuration: Duration(milliseconds: 1000),
                  ),
                  controller: _refreshController,
                  onRefresh: _onPullDownRefresher,
                  onLoading: _onPullUpRefresher,
                  child: _contentWiget()),
            ),
          ],
        ),
      ),
    );
  }

  Widget _contentWiget() {
    return SingleChildScrollView(
      child: Column(
        children: [
          AppDimens.verticalSpace24, //NOTE : MOVIE IMAGE
          BlocConsumer<HomeCubit, HomeState>(
              bloc: _homeCubit..getListMovie(),
              listener: (c, s) {},
              builder: (c, s) {
                if (s is OnDoneHomeNP) {
                  return Padding(
                    padding: const EdgeInsets.only(left: 16.0, right: 16),
                    child: Container(
                      margin: EdgeInsets.only(
                        top: 28,
                      ),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Text(
                                'NOW PLAYING',
                                style: text16WhiteNunitoBold,
                              ),
                            ],
                          ),
                          AppDimens.verticalSpace16,
                          SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: List.generate(s.data!.results!.length,
                                  (index) {
                                var data = s.data!.results![index];
                                return InkWell(
                                  onTap: () {
                                    Navigator.pushNamed(
                                        context, routeDetailMovie,
                                        arguments: data.id);
                                  },
                                  child: MovieCard(
                                    name: data.originalTitle!,
                                    titile: data.voteAverage!.toString(),
                                    imageUrl: "${baseImg}${data.posterPath!}",
                                  ),
                                );
                              }),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                } else {
                  return Center(child: Text(" "));
                }
              }),

          //NOTE : DISNEY
          SizedBox(
            height: 28,
          ),
          Container(
            padding: EdgeInsets.symmetric(
              vertical: 10,
              horizontal: 16,
            ),
            child: BlocConsumer<HomeTrendingCubit, HomeTrendingState>(
              bloc: _homeTrendingCubit,
              listener: (c, s) {
                if (_offset == -0) if (s is OnLoadingHomeTrending) {
                  _onLoadingPopular(s);
                }

                if (s is OnDoneHomeTrending) {
                  _onDonePopular(s);
                }
              },
              builder: (c, s) {
                if (_offset == 0 && s is OnLoadingHomeTrending) {
                  return Center(
                    child: CircularProgressIndicator(
                      color: white,
                    ),
                  );
                }

                if (_offset == 1 && _popularData.isEmpty) {
                  return Center(
                    child: CircularProgressIndicator(
                      color: white,
                    ),
                  );
                }

                return Column(
                  children: [
                    Row(
                      children: [
                        Text(
                          'POPULAR MOVIES',
                          style: text16WhiteNunitoBold,
                        ),
                      ],
                    ),
                    AppDimens.verticalSpace20,
                    Column(
                        children: List.generate(_popularData.length, (index) {
                      var data = _popularData[index];
                      return InkWell(
                        onTap: () {
                          Navigator.pushNamed(context, routeDetailMovie,
                              arguments: data.id);
                        },
                        child: MoviePopular(
                          name: data.originalTitle!,
                          title: data.voteAverage!.toString(),
                          imageUrl: "${baseImg}${data.posterPath!}",
                        ),
                      );
                    })),
                  ],
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  void _onPullDownRefresher() async {
    setState(() {
      _isRefreshing = true;
    });

    _homeTrendingCubit.getListMovieTrending(page: _offset);
  }

  void _onPullUpRefresher() async {
    _homeTrendingCubit.getListMovieTrending(page: _offset);
  }

  void _onLoadingPopular(s) {
    setState(() {
      _popularData = [];
      _isDisableLoadMore = false;
      _offset = 0;
    });
  }

  void _onDonePopular(s) {
    setState(() {
      s.data!.results!.forEach((_data) {
        _popularData.add(_data);
        _offset += 1;
      });
      _refreshController.loadComplete();

      if (s.data!.results!.length == 0) {
        _isDisableLoadMore = true;
      }

      if (_isRefreshing) {
        setState(() {
          _isDisableLoadMore = false;
          _isRefreshing = false;
          _refreshController.refreshCompleted();
        });
      }
    });
  }
}
