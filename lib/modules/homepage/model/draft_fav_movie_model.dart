import 'package:hive/hive.dart';

part 'draft_fav_movie_model.g.dart';

@HiveType(typeId: 1)
class DraftFavMovieModel {
  DraftFavMovieModel(
      {this.id, this.originalTitle, this.rating, this.isFav, this.posterPath});

  @HiveField(0)
  int? id;

  @HiveField(1)
  String? originalTitle;

  @HiveField(2)
  bool? isFav;

  @HiveField(3)
  String? posterPath;

  @HiveField(4)
  String? rating;
}
