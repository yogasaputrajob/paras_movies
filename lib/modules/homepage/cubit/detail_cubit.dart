import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:paras/modules/homepage/model/detail_model.dart';
import 'package:paras/modules/homepage/model/home_nowplaying_model.dart';
import 'package:paras/modules/homepage/service/home_service.dart';

import 'package:hive/hive.dart';

part 'detail_state.dart';

class DetailCubit extends Cubit<DetailState> {
  DetailCubit() : super(DetailInitial());

  final HomeService _homeService = HomeService();
  int lokalIdMovie = 0;

  void getDetailMovie({int? id}) async {
    emit(OnLoadingDetail());

    try {
      var res = await _homeService.getDetailMovie(id: id);

      try {
        if (res.statusCode == 200) {
          print(res.data);
          var _data = DetailModel.fromJson(res.data);
          print('detailMovie Status : Success');
          emit(OnDoneDetail(data: _data));
        } else {
          emit(OnErrorDetail(
              errorCode: res.statusCode,
              errorType: "ListMovie",
              errorMessage: "${res.toString()}"));
        }
      } catch (e) {
        emit(OnErrorDetail(
            errorCode: res.statusCode,
            errorType: "ListMovie",
            errorMessage: "detailMovie " + e.toString()));
      }
    } catch (e) {
      emit(OnErrorDetail(
          errorCode: 502,
          errorType: "ListMovie",
          errorMessage: "detailMovie " + e.toString()));
    }
  }
}
