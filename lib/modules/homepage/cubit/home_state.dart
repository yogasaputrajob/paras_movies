part of 'home_cubit.dart';

@immutable
abstract class HomeState {}

class HomeInitial extends HomeState {}

class OnLoadingHomeNP extends HomeState {}

class OnDoneHomeNP extends HomeState {
  final HomeNowPlayingModel? data;

  OnDoneHomeNP({this.data});
}

class OnErrorHome extends HomeState {
  final int? errorCode;
  final String? errorType;
  final String? errorMessage;

  OnErrorHome({
    this.errorCode,
    this.errorType,
    this.errorMessage,
  });
}
