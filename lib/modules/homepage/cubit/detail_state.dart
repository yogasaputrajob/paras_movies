part of 'detail_cubit.dart';

@immutable
abstract class DetailState {}

class DetailInitial extends DetailState {}

class OnLoadingDetail extends DetailState {}

class OnDoneDetail extends DetailState {
  final DetailModel? data;

  OnDoneDetail({this.data});
}

class OnErrorDetail extends DetailState {
  final int? errorCode;
  final String? errorType;
  final String? errorMessage;

  OnErrorDetail({
    this.errorCode,
    this.errorType,
    this.errorMessage,
  });
}
