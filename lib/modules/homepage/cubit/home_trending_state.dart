part of 'home_trending_cubit.dart';

@immutable
abstract class HomeTrendingState {}

class HomeTrendingInitial extends HomeTrendingState {}

class OnLoadingHomeTrending extends HomeTrendingState {}

class OnDoneHomeTrending extends HomeTrendingState {
  final HomeNowPlayingModel? data;

  OnDoneHomeTrending({this.data});
}

class OnErrorHome extends HomeTrendingState {
  final int? errorCode;
  final String? errorType;
  final String? errorMessage;

  OnErrorHome({
    this.errorCode,
    this.errorType,
    this.errorMessage,
  });
}
