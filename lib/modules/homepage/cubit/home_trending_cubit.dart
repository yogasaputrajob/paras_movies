import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:paras/modules/homepage/model/home_nowplaying_model.dart';
import 'package:paras/modules/homepage/service/home_service.dart';

part 'home_trending_state.dart';

class HomeTrendingCubit extends Cubit<HomeTrendingState> {
  HomeTrendingCubit() : super(HomeTrendingInitial());

  final HomeService _homeService = HomeService();

  void getListMovieTrending({int? page}) async {
    emit(OnLoadingHomeTrending());

    try {
      var res = await _homeService.getListMoviePopular(page: page);

      try {
        if (res.statusCode == 200) {
          print(page);
          var _data = HomeNowPlayingModel.fromJson(res.data);
          print('ListMovie TrendingStatus : Success');
          emit(OnDoneHomeTrending(data: _data));
        } else {
          emit(OnErrorHome(
              errorCode: res.statusCode,
              errorType: "ListMovie",
              errorMessage: "${res.toString()}"));
        }
      } catch (e) {
        emit(OnErrorHome(
            errorCode: res.statusCode,
            errorType: "ListMovie",
            errorMessage: "ListMovie Trending" + e.toString()));
      }
    } catch (e) {
      emit(OnErrorHome(
          errorCode: 502,
          errorType: "ListMovie",
          errorMessage: "ListMovie Trending" + e.toString()));
    }
  }
}
