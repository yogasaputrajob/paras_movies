import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:paras/modules/homepage/model/home_nowplaying_model.dart';
import 'package:paras/modules/homepage/service/home_service.dart';

part 'home_state.dart';

class HomeCubit extends Cubit<HomeState> {
  HomeCubit() : super(HomeInitial());

  final HomeService _homeService = HomeService();

  void getListMovie() async {
    emit(OnLoadingHomeNP());

    try {
      var res = await _homeService.getListMovie();

      try {
        if (res.statusCode == 200) {
          var _data = HomeNowPlayingModel.fromJson(res.data);
          print('ListMovie Status : Success');
          emit(OnDoneHomeNP(data: _data));
        } else {
          emit(OnErrorHome(
              errorCode: res.statusCode,
              errorType: "ListMovie",
              errorMessage: "${res.toString()}"));
        }
      } catch (e) {
        emit(OnErrorHome(
            errorCode: res.statusCode,
            errorType: "ListMovie",
            errorMessage: "ListMovie " + e.toString()));
      }
    } catch (e) {
      emit(OnErrorHome(
          errorCode: 502,
          errorType: "ListMovie",
          errorMessage: "ListMovie " + e.toString()));
    }
  }
}
