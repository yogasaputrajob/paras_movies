import 'dart:io';

import 'package:paras/constants/constants.dart';
import 'package:paras/lib.dart';

import 'package:dio/dio.dart';

class HomeService {
  final Dio _dio = new Dio();

  HomeService() {}

  Future<Response> getListMovie() async {
    _dio.options.baseUrl = baseUrl;

    try {
      var response = await _dio.get(apiNowPlaying, queryParameters: {
        "api_key": tokenKey,
        "language": 'en-US',
        "page": 1
      });
      return response;
    } on DioError catch (e) {
      return DioCatchError(e);
    }
  }

  Future<Response> getListMoviePopular({int? page}) async {
    _dio.options.baseUrl = baseUrl;

    try {
      var response = await _dio.get(apiPopular, queryParameters: {
        "api_key": tokenKey,
        "language": 'en-US',
        "page": page
      });
      return response;
    } on DioError catch (e) {
      return DioCatchError(e);
    }
  }

  Future<Response> getDetailMovie({int? id}) async {
    _dio.options.baseUrl = baseUrl;

    try {
      var response = await _dio.get("${apiDetailMovie}${id}", queryParameters: {
        "api_key": tokenKey,
      });
      return response;
    } on DioError catch (e) {
      return DioCatchError(e);
    }
  }
}
