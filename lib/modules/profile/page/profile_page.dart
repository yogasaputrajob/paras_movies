import 'package:flutter/material.dart';
import 'package:paras/lib.dart';
import 'package:paras/modules/login/services/google_auth_service.dart';

class ProfileScreenPage extends StatefulWidget {
  final int? id;
  ProfileScreenPage({Key? key, this.id}) : super(key: key);

  @override
  State<ProfileScreenPage> createState() => _ProfileScreenPageState();
}

class _ProfileScreenPageState extends State<ProfileScreenPage> {
  bool isExpanded = false;

  @override
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBarWidget(
          title: "Profile",
          textStyle: text24NunitoWhiteBold,
          backgroundColor: bgColor,
        ),
        body: Container(
            width: double.infinity,
            height: double.infinity,
            color: bgColor,
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
              Container(
                width: 150.0,
                height: 150.0,
                decoration: BoxDecoration(
                  color: const Color(0xff7c94b6),
                  image: DecorationImage(
                    image: NetworkImage(GoogleAuthService().user.photoURL!),
                    fit: BoxFit.cover,
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(75.0)),
                  border: Border.all(
                    color: information300,
                    width: 4.0,
                  ),
                ),
              ),
              AppDimens.verticalSpace14,
              Text("${GoogleAuthService().user.displayName}",
                  style: text16WhiteNunitoBold),
              AppDimens.verticalSpace8,
              Text("${GoogleAuthService().user.email}",
                  style: text18NunitoWhiteW500),
              AppDimens.verticalSpace60,
              InkWell(
                onTap: () {
                  GoogleAuthService().signOut();
                },
                child: Padding(
                  padding: const EdgeInsets.only(left: 50.0, right: 50),
                  child: Container(
                    height: 50,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: polri100,
                      ),
                      borderRadius: BorderRadius.circular(10.0),
                      color: Colors.white,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                            // decoration: BoxDecoration(color: Colors.blue),
                            child:
                                Image.network(IMG.GOOGLE, fit: BoxFit.cover)),
                        AppDimens.verticalSpace6,
                        Text(
                          'Logout Dari Google',
                          style: text16HTW600,
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ])));
  }
}
