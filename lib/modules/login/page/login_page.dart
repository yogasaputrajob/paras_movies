import 'package:fluentui_system_icons/fluentui_system_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:paras/lib.dart';
import 'package:lottie/lottie.dart' as lottie;
import 'package:paras/modules/login/cubit/login_cubit.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  LoginCubit _loginCubit = LoginCubit();
  final GlobalKey<ScaffoldState> _scaffoldLoginKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.dark,
      child: WillPopScope(
        onWillPop: onWillPop,
        child: GestureDetector(
            onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
            child: Scaffold(
                key: _scaffoldLoginKey,
                body: BlocConsumer<LoginCubit, LoginState>(
                  bloc: _loginCubit,
                  listener: (c, s) {
                    if (s is OnDoneCreateLogin) {
                      FlushBar.flushBarView(
                          isDismissible: true,
                          duration: 2,
                          context: context,
                          message: "Login Berhasil",
                          textStyle: text14SC500W600,
                          iconColor: success500,
                          icon: Icons.check,
                          callBack: () {
                            Navigator.pushNamed(context, routeHome);
                          });
                    }
                  },
                  builder: (c, s) {
                    return SafeArea(
                      child: Container(
                        height: MediaQuery.of(context).size.height,
                        width: double.infinity,
                        color: bgColor,
                        child: Stack(
                          children: [
                            SafeArea(
                              child: SizedBox(
                                width: double.infinity,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    AppDimens.verticalSpace100,
                                    Text("PARAS MOVIES",
                                        style: text34NunitoWhiteNormal),

                                    AppDimens.verticalSpace100,
                                    lottie.Lottie.asset(
                                      GIF.GIF_MOVIE,
                                      height: 350,
                                      width: 400,
                                    ),

                                    // child: Image.asset('assets/images/img/police_office.jpg')),
                                  ],
                                ),
                              ),
                            ),
                            Positioned(
                              bottom: 40,
                              right: 16,
                              left: 16,
                              child: InkWell(
                                onTap: () {
                                  _loginCubit.loginGoogle();
                                },
                                child: Container(
                                  height: 60,
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      color: polri100,
                                    ),
                                    borderRadius: BorderRadius.circular(15.0),
                                    color: Colors.white,
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Container(
                                          // decoration: BoxDecoration(color: Colors.blue),
                                          child: Image.network(IMG.GOOGLE,
                                              fit: BoxFit.cover)),
                                      SizedBox(
                                        width: 5.0,
                                      ),
                                      Text(
                                        'Masuk Menggunakan Google',
                                        style: text16HTW600,
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ))),
      ),
    );
  }
}
