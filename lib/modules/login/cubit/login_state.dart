part of 'login_cubit.dart';

@immutable
abstract class LoginState {}

class LoginInitial extends LoginState {}

class OnLoadingLogin extends LoginState {}

class OnDoneCreateLogin extends LoginState {}

class OnErrorLogin extends LoginState {
  final int? errorCode;
  final String? errorType;
  final String? errorMessage;

  OnErrorLogin({
    this.errorCode,
    this.errorType,
    this.errorMessage,
  });
}
