import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';
import 'package:paras/modules/login/services/google_auth_service.dart';

part 'login_state.dart';

class LoginCubit extends Cubit<LoginState> {
  LoginCubit() : super(LoginInitial());
  GoogleAuthService _googleAuthService = GoogleAuthService();

  void loginGoogle() async {
    emit(OnLoadingLogin());
    await _googleAuthService.signInWithGoogle();
    emit(OnDoneCreateLogin());
  }

  void logoutGoogle() async {
    emit(OnLoadingLogin());
    await _googleAuthService.signOut();
    emit(OnDoneCreateLogin());
  }
}
