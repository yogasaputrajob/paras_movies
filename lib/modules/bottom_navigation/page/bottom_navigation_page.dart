import 'package:fluentui_system_icons/fluentui_system_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:paras/lib.dart';
import 'package:paras/modules/bottom_navigation/cubit/bottom_navigation_cubit.dart';
import 'package:paras/modules/bottom_navigation/dto/nav_enum_dto.dart';
import 'package:paras/modules/homepage/page/favoritepage.dart';
import 'package:paras/modules/profile/page/page.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';

import 'dart:io' show Platform;

class BottomNav extends StatefulWidget {
  @override
  _BottomNavState createState() => _BottomNavState();
}

class _BottomNavState extends State<BottomNav> {
  BottomNavigationCubit _bottomNavigationCubit = BottomNavigationCubit();
  final GlobalKey<CurvedNavigationBarState> _bottomNavigationKey = GlobalKey();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onWillPop,
      child: Scaffold(
        bottomNavigationBar:
            BlocBuilder<BottomNavigationCubit, BottomNavigationState>(
          builder: (context, state) {
            return CurvedNavigationBar(
              index: state.index,
              key: _bottomNavigationKey,
              height: 60.0,
              items: [
                bottomItem(
                    title: "Home",
                    index: 0,
                    cIndex: state.index,
                    icon: FluentIcons.home_20_filled),
                bottomItem(
                    title: "Favorites",
                    index: 1,
                    cIndex: state.index,
                    icon: FluentIcons.heart_20_filled),
                bottomItem(
                    title: "Profile",
                    index: 2,
                    cIndex: state.index,
                    icon: FluentIcons.person_20_filled),
              ],
              color: Color(0xFF19182C).withOpacity(0.95),
              buttonBackgroundColor: Colors.amber.withOpacity(0.4),
              backgroundColor: Color(0xFF19182C).withOpacity(0.8),
              animationCurve: Curves.easeInOut,
              animationDuration: const Duration(milliseconds: 400),
              onTap: (index) {
                if (index == 0) {
                  BlocProvider.of<BottomNavigationCubit>(context)
                      .getNavBarItem(NavbarItem.home);
                } else if (index == 1) {
                  BlocProvider.of<BottomNavigationCubit>(context)
                      .getNavBarItem(NavbarItem.favorites);
                } else {
                  BlocProvider.of<BottomNavigationCubit>(context)
                      .getNavBarItem(NavbarItem.profile);
                }
              },
            );
          },
        ),
        body: BlocBuilder<BottomNavigationCubit, BottomNavigationState>(
            builder: (context, state) {
          if (state.navbarItem == NavbarItem.home) {
            return HomePage();
          } else if (state.navbarItem == NavbarItem.profile) {
            return ProfileScreenPage();
          } else {
            return FavoriteScreenPage();
          }
        }),
      ),
    );
  }

  Widget bottomItem(
      {required int index,
      required int cIndex,
      required String title,
      required IconData icon}) {
    if (index == cIndex) {
      return Icon(
        icon,
        size: 26,
        color: white,
      );
    } else {
      return Padding(
          padding: const EdgeInsets.only(top: 6.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                icon,
                size: 22,
                color: Colors.white,
              ),
              const SizedBox(height: 5),
              Text(
                title,
                style: text14whiteHTW600,
              )
            ],
          ));
    }
  }
}
