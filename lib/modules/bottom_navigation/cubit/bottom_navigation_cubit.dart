import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:paras/modules/bottom_navigation/dto/nav_enum_dto.dart';

part 'bottom_navigation_state.dart';

class BottomNavigationCubit extends Cubit<BottomNavigationState> {
  BottomNavigationCubit() : super(BottomNavigationState(NavbarItem.home, 0));

  void getNavBarItem(NavbarItem navbarItem) {
    switch (navbarItem) {
      case NavbarItem.home:
        emit(BottomNavigationState(NavbarItem.home, 0));
        break;

      case NavbarItem.profile:
        emit(BottomNavigationState(NavbarItem.profile, 2));
        break;

      case NavbarItem.favorites:
        emit(BottomNavigationState(NavbarItem.favorites, 1));
        break;
    }
  }
}
