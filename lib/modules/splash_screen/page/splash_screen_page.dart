import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:paras/modules/bottom_navigation/cubit/bottom_navigation_cubit.dart';
import 'package:paras/modules/bottom_navigation/page/bottom_navigation_page.dart';
import 'package:paras/modules/login/services/google_auth_service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:paras/lib.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart' as lottie;

class SplashScreenPage extends StatefulWidget {
  @override
  _SplashScreenPageState createState() => _SplashScreenPageState();
}

class _SplashScreenPageState extends State<SplashScreenPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnimatedSplashScreen(
          splashIconSize: MediaQuery.of(context).size.height,
          duration: 1400,
          splash: SafeArea(
              child: Container(
            color: bgColor,
            height: MediaQuery.of(context).size.height,
            width: double.infinity,
            child: Stack(
              children: [
                SafeArea(
                  child: SizedBox(
                    width: double.infinity,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        AppDimens.verticalSpace100,
                        AppDimens.verticalSpace100,
                        FlutterLogo(
                          size: 350,
                        ),
                      ],
                    ),
                  ),
                ),

                // _checkWidgetStatusLogin(s),
              ],
            ),
          )),
          nextScreen: StreamBuilder<User?>(
            stream: GoogleAuthService().authState,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              } else if (snapshot.connectionState == ConnectionState.done ||
                  snapshot.connectionState == ConnectionState.active) {
                if (snapshot.hasData) {
                  print(snapshot.hasData);
                  return BlocProvider<BottomNavigationCubit>(
                      create: (context) => BottomNavigationCubit(),
                      child: BottomNav());
                  // return LoginPage();
                } else {
                  return LoginPage();
                }
              } else {
                return Center(
                  child: Text('State: ${snapshot.connectionState}'),
                );
              }
            },
          ),
          splashTransition:
              SplashTransition.fadeTransition) /* add child content here */,
    );
  }
}
