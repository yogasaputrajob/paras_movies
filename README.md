# Paras Movies

* Project Ini menggunakan State Management Bloc Pattern (Cubit)
* Flutter version 3.3.1
* VSCode
* Emulator (Nox)


## Detail Aplikasi

* Login Menggunakan Google (Firebase / flutterfire CLI)
* Favorite Movies (penyimpanan Lokal)
* Home Movie
* Detail Movie
* Profile

API yang digunakan di project ini:

- [tmdb:Open source API Movie](https://www.themoviedb.org/documentation/api/sessions)
- [Nowplaying Movies : Api path ](https://developers.themoviedb.org/3/movies/get-now-playing)
- [Popular Movies : Api path ](https://developers.themoviedb.org/3/movies/get-popular-movies)
- [Detail Movies : Api path ](https://developers.themoviedb.org/3/movies/get-movie-details)


Untuk detail tentang aplikasi : 
email : yogasaputrajob@gmail.com

![splasscreen](assets/images/photo_2022-11-25_14-24-20.jpg)
![login](assets/images/photo_2022-11-25_14-24-25.jpg)
![home](assets/images/photo_2022-11-25_14-24-27.jpg)
![favorites](assets/images/photo_2022-11-25_14-24-31.jpg)
![detail](assets/images/photo_2022-11-25_14-24-34.jpg)
![profile](assets/images/photo_2022-11-25_14-24-37.jpg)

